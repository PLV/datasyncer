#!/bin/bash

homepath=$(cd `dirname $0`/..; pwd)
cd $homepath
pid_file="$homepath/bin/.pid"
if [ -f "$pid_file" ]; then
    pid=`cat $pid_file`
    kill -9 $pid
    echo "datasync stopped successful"
else
    echo "datasync server not started"
fi