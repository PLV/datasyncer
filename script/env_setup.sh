#!/bin/bash

homepath=$(cd `dirname $0`/..; pwd)
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

yum install epel-release -y
yum install python-pip -y

mkdir ~/.pip
echo \
"[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple/

[install]
trusted-host=pypi.tuna.tsinghua.edu.cn" > ~/.pip/pip.conf

yum install MySQL-python -y
pip install --upgrade pip
pip install -r $homepath/requirements.txt