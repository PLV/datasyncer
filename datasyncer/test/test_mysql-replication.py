# -*- coding: UTF-8 -*-
from pymysqlreplication import BinLogStreamReader
import time

mysql_settings = {'host': '192.168.0.192', 'port': 3306, 'user': 'root', 'passwd': 'root'}

stream = BinLogStreamReader(connection_settings=mysql_settings, server_id=100)
event = stream.fetchone()
if event:
    event.dump()

# for binlogevent in stream:
#     binlogevent.dump()

stream.close()
