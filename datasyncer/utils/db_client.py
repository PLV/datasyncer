# -*- coding: UTF-8 -*-

import itertools
from sqlalchemy import *
from sqlalchemy.orm import *


class Client(object):
    def __init__(self, database, user, password, host, db):
        CONNECT_STRING = '{db}://{0}:{1}@{2}/{3}'.format(user, password, host, database, db=db)
        self.source_engine = create_engine(CONNECT_STRING, echo=True)

    def query(self, _query, *parameters):
        """Returns a row list for the given query and parameters."""
        DB_Session = sessionmaker(self.source_engine)
        _session = DB_Session(autocommit=True, autoflush=True)
        try:
            cursor = _session.execute(_query, parameters).cursor
            column_names = [d[0] for d in cursor.description]
            return [Row(itertools.izip(column_names, row)) for row in cursor]
        finally:
            _session.close()

    def execute(self, _query, *parameters):
        DB_Session = sessionmaker(self.source_engine)
        _session = DB_Session(autocommit=True, autoflush=True)
        try:
            _session.execute(_query, parameters)
        finally:
            _session.close()

    def drop_table(self, table):
        DB_Session = sessionmaker(self.source_engine)
        _session = DB_Session(autocommit=True, autoflush=True)
        try:
            _session.execute("DROP TABLE '{0}'".format(table))
        finally:
            _session.close()


class Row(dict):
    """A dict that allows for object-like property access syntax."""

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)
